module.exports = (context) => {

    // Plugins to load
    const plugins = {
        "autoprefixer": {}
    };

    return {
        plugins: plugins
    };
};