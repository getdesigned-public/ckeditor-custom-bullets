TYPO3 CMS Extension: `ckeditor_custom_bullets`
==============================================

## 1 Features

* Hide bullets from individual list items
* Custom bullet for individual list items of ordered lists

## 2 Usage

### 2.1 Installation

#### Installation using Composer

The recommended way to install the extension is using [Composer][1].

Run the following command within your Composer based TYPO3 CMS project:

```shell
composer require getdesigned/ckeditor-custom-bullets
```

#### Installation as extension from TYPO3 Extension Repository (TER)

Download and install the [extension][2] with the extension manager module.

### 2.2 Minimal setup

* Simply install the extension and you are good to go.
* **Optional:** Include the extension's static TypoScript to include base CSS styles for the frontend.

[1]: https://getcomposer.org/
[2]: https://extensions.typo3.org/extension/ckeditor_custom_bullets