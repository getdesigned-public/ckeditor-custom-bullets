<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Custom Bullets for CKEditor',
    'description' => 'Allows editors to set custom bullets for each list item in CKEditor',
    'category' => 'fe',
    'version' => '1.0.1',
    'state' => 'stable',
    'author' => 'Daniel Haring',
    'author_company' => 'Getdesigned GmbH',
    'author_email' => 'dh@getdesigned.at',
    'clearCacheOnLoad' => true,
    'constraints' => [
        'depends' => [
            'typo3' => '10.4.0-10.99.99',
        ]
    ]
];