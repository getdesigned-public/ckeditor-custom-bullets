/**
 * The dialog for custom bullets.
 *
 * @author Daniel Haring <dh@getdesigned.at>
 * @since 1.0.0
 */
(function() {

    /**
     * Generates a closure for retrieving the selected list item.
     *
     * @param {CKEDITOR.editor} editor The CKEditor instance
     * @returns {function()} The closure generated
     */
    function generateModelClosure(editor) {
        return function() {
            return getListItem(editor) || null;
        };
    }

    /**
     * Retrieves the selected list item, if any.
     *
     * @param {CKEDITOR.editor} editor The CKEditor instance
     * @returns {null|*}
     */
    function getListItem(editor) {
        try {
            var range = editor.getSelection().getRanges()[0];
        } catch (e) {
            return null;
        }
        range.shrink(CKEDITOR.SHRINK_TEXT);
        return editor.elementPath(range.getCommonAncestor()).contains('li', 1);
    }

    /**
     * Creates the dialog.
     *
     * @param {CKEDITOR.editor} editor The CKEditor instance
     * @returns {{minHeight: number, getModel: (function()), onShow: onShow, contents: [{accessKey: string, elements: [{children: [{commit: commit, setup: setup, style: string, label: *, id: string, type: string, items: ((*|string)[]|(*|string)[])[]}, {default: string, commit: commit, setup: setup, label: *, id: string, type: string}], widths: string[], type: string}], id: string}], minWidth: number, title, onOk: onOk}} The dialog configuration created
     */
    function listItemStyle(editor) {
        var lang = editor.lang.listitemstyle;
        return {
            title: lang.title,
            minWidth: 300,
            minHeight: 50,
            getModel: generateModelClosure(editor),
            contents: [
                {
                    id: 'info',
                    accessKey: 'I',
                    elements: [
                        {
                            type: 'hbox',
                            widths: [ '100%' ],
                            children: [
                                {
                                    type: 'select',
                                    label: lang.bullet,
                                    id: 'bullet',
                                    style: 'width: 100%;',
                                    items: [
                                        [lang.inherit, ''],
                                        [lang.none, 'none'],
                                        [lang.custom, 'custom']
                                    ],
                                    setup: function(element) {
                                        var value = element.getAttribute('data-bullet') || '';
                                        this.setValue(value);
                                    },
                                    commit: function(element) {
                                        var value = this.getValue();
                                        if (value) {
                                            element.setAttribute('data-bullet', value);
                                        } else {
                                            element.removeAttribute('data-bullet');
                                        }
                                    }
                                },
                                {
                                    type: 'text',
                                    label: lang.customBullet,
                                    id: 'customBullet',
                                    default: '',
                                    setup: function(element) {
                                        var value = element.getAttribute('data-custom-bullet') || '';
                                        this.setValue(value);
                                    },
                                    commit: function(element) {
                                        var value = this.getValue();
                                        if (value) {
                                            element.setAttribute('data-custom-bullet', value);
                                        } else {
                                            element.removeAttribute('data-custom-bullet');
                                        }
                                    }
                                }
                            ]
                        }
                    ]
                }
            ],
            onShow: function() {
                var editor = this.getParentEditor(),
                    element = getListItem(editor);
                element && this.setupContent(element);
            },
            onOk: function () {
                var editor = this.getParentEditor(),
                    element = getListItem(editor);
                element && this.commitContent(element);
            }
        };
    }

        // register dialog
    CKEDITOR.dialog.add('listItemStyle', function (editor) {
        return listItemStyle(editor);
    });
})();