/**
 * Adds a dialog for list items.
 *
 * @author Daniel Haring <dh@getdesigned.at>
 * @since 1.0.0
 */
CKEDITOR.plugins.add('listitemstyle', {
    requires: 'dialog,contextmenu',
    lang: 'de,en',
    init: function(editor) {
        if (editor.blockless) {
            return;
        }
        var def = new CKEDITOR.dialogCommand('listItemStyle', {
                requiredContent: 'li',
                allowedContent: 'li[data-bullet]'
            }),
            cmd = editor.addCommand('listItemStyle', def);
        editor.addFeature(cmd);
        CKEDITOR.dialog.add('listItemStyle', this.path + 'dialogs/listitemstyle.js');

        editor.addMenuGroup('listItem');
        editor.addMenuItem('listItemStyles', {
            label: editor.lang.listitemstyle.title,
            group: 'listItem',
            command: 'listItemStyle'
        });

        editor.contextMenu.addListener(function(element) {
            if (element && element.getAscendant('li', true)) {
                return {listItemStyles: CKEDITOR.TRISTATE_OFF};
            }
            return null;
        });
    }
});