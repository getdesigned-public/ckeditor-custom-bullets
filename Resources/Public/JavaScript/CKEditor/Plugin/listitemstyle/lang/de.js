CKEDITOR.plugins.setLang('listitemstyle', 'de', {
    bullet: 'Aufzählungszeichen',
    custom: 'Eigenes …',
    customBullet: 'Benutzerdefiniertes Zeichen',
    inherit: 'Vererbt',
    none: 'Keines',
    title: 'Listenelement Eigenschaften'
});