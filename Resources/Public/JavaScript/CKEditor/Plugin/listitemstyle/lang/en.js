CKEDITOR.plugins.setLang('listitemstyle', 'en', {
    bullet: 'Bullet',
    custom: 'Custom …',
    customBullet: 'Custom bullet',
    inherit: 'Inherit',
    none: 'None',
    title: 'List Item Properties'
});