/**
 * The JavaScript module.
 *
 * @author Daniel Haring <dh@getdesigned.at>
 * @since 1.0.0
 */

// Stylesheets
import '../Stylesheets/custom-bullets.scss';