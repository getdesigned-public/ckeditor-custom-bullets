.. include:: Includes.txt

.. _start:

=============================================================
Custom Bullets for CKEditor
=============================================================

.. only:: html

   :Version:
      |release|

   :Language:
      en

   :Copyright:
      2021

   :Authors:
      Daniel Haring

   :License:
      This extension documentation is published under the
      `CC BY-NC-SA 4.0 <https://creativecommons.org/licenses/by-nc-sa/4.0/>`__ (Creative Commons)
      license

   :Rendered:
      |today|

   The content of this document is related to TYPO3 CMS,
   a GNU/GPL CMS/Framework available from `typo3.org <https://typo3.org/>`_ .

   **Table of Contents**

.. toctree::
   :maxdepth: 3
   :titlesonly:
   :glob:

   Introduction/Index
   Installation/Index
   Editor/Index
   KnownProblems/Index
