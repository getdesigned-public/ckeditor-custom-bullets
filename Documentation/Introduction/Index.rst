.. include:: ../Includes.txt

.. _introduction:

============
Introduction
============

.. _what-it-does:

What does it do?
================

This extension allows manipulating individual list items within *CKEditor*. It enables editors to:

* hide individual bullets
* set custom bullets of ordered lists

It also works well alongside the *CKEditor* plugin `List Style <https://ckeditor.com/cke4/addon/liststyle>`__, which is
not shipped with this extension.

.. _screenshots:

Screenshots
===========

.. figure:: ../Images/UserManual/ListItemContextMenu.png
   :class: with-shadow
   :alt: List item context menu

   Additional context menu option for list items.

.. figure:: ../Images/UserManual/ListItemProperties.png
   :class: with-shadow
   :alt: List item properties

   Properties for individual list items.