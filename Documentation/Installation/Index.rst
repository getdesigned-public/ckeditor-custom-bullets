.. include:: ../Includes.txt

.. _installation:

============
Installation
============

This extension assumes that your TYPO3 CMS installation uses *CKEditor* as text editor. Please make sure that *CKEditor*
is installed and working properly.

Since *CKEditor* is the default text editor since TYPO3 CMS v8, the extension should work out-of-the-box for most TYPO3
CMS installations.

.. _installation-get-extension:

Get the extension
=================

Through Composer (recommended)
------------------------------

The recommended way to install the extension is using `Composer <https://getcomposer.org/>`__.

Simply run the following command in the root directory of your TYPO3 CMS project:

.. code-block:: shell

   composer require getdesigned/ckeditor-custom-bullets

Through the Extension Manager
-----------------------------

.. important::

   This method works only if your system has **not** been set to Composer mode.

#. Switch to the "Extensions" module
#. Press the "Retrieve/Update" button
#. Look for the "ckeditor_custom_bullets" extension key
#. Press the corresponding download button

Through typo3.org
-----------------

.. important::

   This method works only if your system has **not** been set to Composer mode.

#. Visit `extensions.typo3.org/extension/ckeditor_custom_bullets
   <https://extensions.typo3.org/extension/ckeditor_custom_bullets>`__
#. Download the t3x or zip version of the extension
#. Upload the downloaded file via the extension manager

Through Git
-----------

You can get a specific version of the extension directly from Git. To do this, change to the extension directory of your
TYPO3 CMS installation (e.g. *~/public/typo3conf/ext/*) and execute the following commands:

.. code-block:: shell

   git clone --depth 1 --branch v1.0.0 https://gitlab.com/getdesigned-public/ckeditor-custom-bullets.git ckeditor_custom_bullets
   rm -rf ckeditor_custom_bullets/.git

.. _installation-activate:

Activate the extension
======================

After you have installed the extension, make sure that it is activated.

To do this, go to the "Extensions" module, search for "Custom Bullets for CKEditor" and activate the extension if it is
not already active.

.. _installation-static-template:

Include the static TypoScript (optional)
========================================

If you want to use the provided basic CSS styles for the frontend, please include the static TypoScript in your
template.

#. Switch to the "Template" module
#. Select the root page of your site
#. Select "Info/Modify" in the upper selection menu
#. Press the "Edit the whole template record" button
#. Switch to the "Includes" tab
#. Add the entry "Custom Bullets CSS (optional) (ckeditor_custom_bullets)" in the "Include static (from extensions)"
   section

.. figure:: ../Images/AdministratorManual/StaticTemplate.png
   :class: with-shadow
   :alt: Static template

   Include the static template.