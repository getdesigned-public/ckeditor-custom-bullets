.. include:: ../Includes.txt

.. _known-problems:

==============
Known Problems
==============

.. _known-problems-ie-lte8-custom-bullets:

Custom bullets in Internet Explorer 8 or lower
==============================================

Custom bullets are set using the CSS notation `attr()`, which is not supported by older versions of Internet Explorer
(8 or lower).

If you want to support said browsers, please consider a JavaScript solution.

See `caniuse.com/css-gencontent <https://caniuse.com/css-gencontent>`__.

.. _known-problems-ul-custom-bullets:

"Custom bullet" input field also visible for unordered lists
============================================================

Currently, the "Custom bullet" input field is also visible for unordered lists, even if the corresponding setting has no
effect.

The cause lies in the current structure of the dialog. A corresponding solution is planned for a future release.