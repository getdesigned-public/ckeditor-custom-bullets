.. include:: ../Includes.txt

.. _for-editors:

===========
For Editors
===========

Whenever you work with lists in the Rich Text Editor of the TYPO3 CMS backend, it is possible for you to manipulate the
bullets of individual list elements.

You can hide individual bullets or set a custom bullet (the latter only for ordered lists).

.. _editor-hiding-a-bullet:

Hiding a bullet
===============

#. Right-click on the desired list item
#. In the context menu that appears, select "List Item Properties"
#. Select "None" as the bullet character
#. Confirm with "OK"

.. _editor-custom-bullet:

Setting a custom bullet
=======================

#. Right-click on the desired list item
#. In the context menu that appears, select "List Item Properties"
#. Select "Custom" as the bullet character
#. Enter the desired bullet in the "Custom bullet" input field
#. Confirm with "OK