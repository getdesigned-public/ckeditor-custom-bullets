const path = require('path');

module.exports = {
    mode: 'production',
    entry: {
        main: {
            import: './Resources/Private/JavaScript/Module.js',
            filename: 'JavaScript/module.js'
        }
    },
    output: {
        path: path.resolve(__dirname, 'Resources/Public')
    },
    module: {
        rules: [
            {
                test: /\.js$/i,
                exclude: /(node_modules)/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env']
                        }
                    }
                ]
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].css',
                            outputPath: (url, resourcePath, context) => {
                                return `Stylesheets/${url}`;
                            }
                        }
                    },
                    {
                        loader: 'extract-loader'
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            url: false
                        }
                    },
                    {
                        loader: 'postcss-loader'
                    },
                    {
                        loader: 'sass-loader'
                    }
                ]
            }
        ]
    }
};