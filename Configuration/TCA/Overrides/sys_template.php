<?php

/*
 * Copyright notice
 *
 * (c) 2021 Getdesigned GmbH <office@getdesigned.at>
 *
 * All rights reserved
 *
 * This script is part of the "Custom Bullets for CKEditor" extension for
 * TYPO3 CMS. The complete licence information can be found in the LICENSE.txt
 * file, which is included with the source code.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */

defined('TYPO3_MODE') or die ('Access denied.');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'ckeditor_custom_bullets',
    'Configuration/TypoScript/Dispatch',
    'Custom Bullets CSS (optional)'
);